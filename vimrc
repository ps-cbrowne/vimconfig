" Setup syntax and colors
syntax on
set t_Co=256
colorscheme mustang

" Configure tabs
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set autoindent

" Configure the vim HUD
set number
set showcmd
set cursorline
set wildmenu
set lazyredraw
set nowrap

" Configure search
set showmatch
set incsearch
set hlsearch

" Highlight long lines
highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
match OverLength /\%101v.\+/

" Automaticly remove trailing whitespece on save
autocmd BufWritePre * %s/\s\+$//e

" Import pathogen
execute pathogen#infect()
