# How do I get set up? #

Be sure you want to. This will remove your existing vim configuration.

```
#!bash
rm -rf ~/.vim ~/.vimrc
git clone git@bitbucket.org:ps-cbrowne/vimconfig.git ~/.vim
cd ~/.vim
git submodule init
git submodule update
```